#pragma once

#include <cassert>
#include <cstdio>
#include <iostream>
#include <memory>
#include <string>
#include <cstring>
#include <sstream>
#include <map>
#include "koopa.h"
using namespace std;
// #define _DEBUG
#define REG_CNT 15

static string tempRegName[REG_CNT] = {"t0", "t1", "t2", "t3", "t4", "t5", "t6", "a0", "a1", "a2", "a3", "a4", "a5", "a6", "a7"};
static map<long, int> insts_table;
static map<long, string> var_table;
static bool regVisited[REG_CNT] = {};
static long stack_shift = 0;
static string cur_func;
static int func_space;
static map<long, string> global_var;
static int global_var_cnt = 0;


static string epilogue;

// 函数声明
void Visit(const koopa_raw_program_t&);
void Visit(const koopa_raw_slice_t&);
void Visit(const koopa_raw_function_t&);
void Visit(const koopa_raw_basic_block_t&);
void Visit(const koopa_raw_value_t&);
void Visit(const koopa_raw_return_t&);
void Visit(const koopa_raw_integer_t&);
void Visit(const koopa_raw_binary_t&);
void VisitLocalAlloc(const koopa_raw_global_alloc_t&);
void Visit(const koopa_raw_store_t&);
void Visit(const koopa_raw_load_t&);
void Visit(const koopa_raw_branch_t&);
void Visit(const koopa_raw_jump_t&);
void Visit(const koopa_raw_call_t&);
void VisitGlobalAlloc(const koopa_raw_global_alloc_t&);

string getStoredAddr(long);
string getNextReg();
void releaseReg(string);
string getRegName(const koopa_raw_value_t&);

string getNextReg() {
  for(int i = 0; i < REG_CNT; i++) {
    if(!regVisited[i]) {
      regVisited[i] = true;
      return tempRegName[i];
    }
  }
  assert(false);
  return "Wrong!";
}

void releaseReg(string reg_name) {
  if(!strcmp(reg_name.c_str(), "x0")) 
    return;
  for(int i = 0; i < REG_CNT; i++) {
    if(!strcmp(reg_name.c_str(), tempRegName[i].c_str()) && regVisited[i]) {
      regVisited[i] = false;
      return;
    }
  }
  // cout << reg_name << "not allocated" << endl;
  // assert(false);
}

bool is_global(long ptr) {
  return global_var.count(ptr);
}

string getStoredAddr(long ptr) {
  string regName = "Wrong\n";
  if(is_global(ptr)) {
    string temp_reg = getNextReg();
    cout << "\tla\t\t" << temp_reg << ",\t" << global_var[ptr] << endl;
    regName = temp_reg;
  }
  else if(var_table.count(ptr)) {
    regName = var_table[ptr];
  }
  else {
    regName = to_string(stack_shift) + "(sp)";
    var_table[ptr] = regName;
    stack_shift += 4;
  }
  return regName;
}

// 获取临时使用的整数，或者是之前用过的指令结果存储的地方
string getRegName(const koopa_raw_value_t &value) {
  const auto &kind = value->kind;
  string regName = "Wrong\n";
  switch (kind.tag) {
    case KOOPA_RVT_INTEGER: {
      if (kind.data.integer.value) {
        regName = getNextReg();
        cout << "\tli\t\t" << regName  << ",\t" << kind.data.integer.value << endl;
      }
      else {
        regName = "x0";
      }
      break;
    }
    case KOOPA_RVT_CALL:
    case KOOPA_RVT_ALLOC:
    case KOOPA_RVT_LOAD:
    case KOOPA_RVT_BINARY: {
      regName = getNextReg();
      long ptr = (long)(&kind.data);
      string varName = getStoredAddr(ptr);
      if(is_global(ptr)) {
        cout << "\tlw\t\t" << regName << ",\t" << "0(" << varName << ")" << endl;
        releaseReg(varName);
      }
      else {
        cout << "\tlw\t\t" << regName << ",\t" << varName << endl;
      }
      break;
    }
    case KOOPA_RVT_FUNC_ARG_REF:{
      int i = kind.data.func_arg_ref.index;
      if (i < 8){
        regName = "a" + to_string(i);
      }
      else{
        int offset = 4 * (i - 8) + func_space;
        regName = getNextReg();
        cout << "\tlw\t\t" << regName << ",\t" << offset << "(sp)" << endl;
      }
      break;
    }
    default:
      cout << "false val!!!" << kind.tag << endl;
      assert(false);
  }
  return regName;
}

int calc_local_var_space(const koopa_raw_basic_block_t&);
int calc_local_var_space(const koopa_raw_slice_t&);
int calc_local_var_space(const koopa_raw_value_t&);
int calc_passed_param_space(const koopa_raw_basic_block_t&);
int calc_passed_param_space(const koopa_raw_slice_t&);
int calc_passed_param_space(const koopa_raw_value_t&);

int calc_local_var_space(const koopa_raw_basic_block_t &bb) {
  return calc_local_var_space(bb->insts);
}

int calc_local_var_space(const koopa_raw_slice_t &slice) {
  int local_var_cnt = 0;
  for (int i = 0; i < int(slice.len); ++i) {
    auto ptr = slice.buffer[i];
    switch (slice.kind) {
      case KOOPA_RSIK_BASIC_BLOCK:
        // 访问基本块
        local_var_cnt += calc_local_var_space(reinterpret_cast<koopa_raw_basic_block_t>(ptr));
        break;
      case KOOPA_RSIK_VALUE:
        // 访问指令
        local_var_cnt += calc_local_var_space(reinterpret_cast<koopa_raw_value_t>(ptr));
        break;
      default:
        // cout << "false slice val!!!" << slice.kind << endl;
        assert(false);
    }
  }
  return local_var_cnt;
}

int calc_local_var_space(const koopa_raw_value_t &value) {
  const auto &kind = value->kind;
  if(kind.tag == KOOPA_RVT_CALL) return 2;
  else {
    if(value->ty->tag == KOOPA_RTT_UNIT) {
      return 0;
    }
    else {
      return 1;
    }
  }
}

int calc_passed_param_space(const koopa_raw_basic_block_t &bb) {
  return calc_passed_param_space(bb->insts);
}

int calc_passed_param_space(const koopa_raw_slice_t &slice) {
  int max_param_num = 0;
  for (int i = 0; i < int(slice.len); ++i) {
    auto ptr = slice.buffer[i];
    switch (slice.kind) {
      case KOOPA_RSIK_BASIC_BLOCK:
        // 访问基本块
        max_param_num  = max(calc_passed_param_space(reinterpret_cast<koopa_raw_basic_block_t>(ptr)), max_param_num);
        break;
      case KOOPA_RSIK_VALUE:
        // 访问指令
        max_param_num = max(calc_passed_param_space(reinterpret_cast<koopa_raw_value_t>(ptr)), max_param_num);
        break;
      default:
        // cout << "false slice val!!!" << slice.kind << endl;
        assert(false);
    }
  }
  return max_param_num;
}

int calc_passed_param_space(const koopa_raw_value_t &value) {
  const auto &kind = value->kind;
  if(kind.tag == KOOPA_RVT_CALL) {
    int param_cnt = kind.data.call.args.len;
    if(param_cnt > 8) {
      return param_cnt - 8;
    }
  }
  return 0;
}


// 访问 raw program
void Visit(const koopa_raw_program_t &program) {
  // 执行一些其他的必要操作
  // ...
  // 访问所有全局变量
  #ifdef _DEBUG
  cout << "visit program { \n";
  #endif

  Visit(program.values);
  // 访问所有函数
  Visit(program.funcs);

  #ifdef _DEBUG
  cout << " }\n";
  #endif
}

// 访问 raw slice
void Visit(const koopa_raw_slice_t &slice) {
  for (int i = 0; i < slice.len; ++i) {
    auto ptr = slice.buffer[i];
    // 根据 slice 的 kind 决定将 ptr 视作何种元素
    #ifdef _DEBUG
    cout << "slice " << i << ": " << slice.kind << endl;
    #endif
    switch (slice.kind) {
      case KOOPA_RSIK_FUNCTION: {
        // 访问函数
        Visit(reinterpret_cast<koopa_raw_function_t>(ptr));
        break;
      }
      case KOOPA_RSIK_BASIC_BLOCK:
        // 访问基本块
        Visit(reinterpret_cast<koopa_raw_basic_block_t>(ptr));
        break;
      case KOOPA_RSIK_VALUE:
        // 访问指令
        Visit(reinterpret_cast<koopa_raw_value_t>(ptr));
        break;
      default:
        // cout << "false slice val!!!" << slice.kind << endl;
        assert(false);
    }
  }
}

// 访问函数
void Visit(const koopa_raw_function_t &func) {
  // 执行一些其他的必要操作
  // 访问所有基本块
  #ifdef _DEBUG
  cout << "visit func:\n";
  Visit(func->bbs);
  #endif
  if(func->bbs.len == 0) return;
  string nameOut(func->name);
  cout << "\t.text\n";
  cout << "\t.globl " << nameOut.substr(1) << endl;
  cur_func = nameOut.substr(1);
  cout << nameOut.substr(1) << ":\n";
  
  // size_t func_len = (func->bbs.len) * 4;
  int local_var_cnt = calc_local_var_space(func->bbs);
  int params_cnt = calc_passed_param_space(func->bbs);
  int func_len = (local_var_cnt + params_cnt + 1) * 4;
  // 16字节对齐
  func_len = int((func_len + 15) / 16) * 16;
  func_space = func_len;
  
  // 更新栈帧
  cout << "\taddi\tsp,\tsp,\t" << -func_len << endl;
  cout << "\tsw\t\tra,\t" << func_len - 4 << "(sp)" << endl;

  epilogue = "\tlw\t\tra,\t"+to_string(func_len - 4)+"(sp)\n";
  epilogue += "\taddi\tsp,\tsp,\t"+to_string(func_len)+"\n";
  
  stack_shift = params_cnt * 4;
  Visit(func->bbs);
}

// 访问基本块
void Visit(const koopa_raw_basic_block_t &bb) {
  // 执行一些其他的必要操作
  // 访问所有指令
  #ifdef _DEBUG
  cout << "visit basic block: \n";
  Visit(bb->insts);
  #endif
  if(strcmp(bb->name, "%entry")) {
    cout << ((string)bb->name).substr(1) << ":\n";
  }
  Visit(bb->insts);
}

// 访问指令
void Visit(const koopa_raw_value_t &value) {
  // 根据指令类型判断后续需要如何访问
  #ifdef _DEBUG
  cout << "visit value: " << value->kind.tag << "\n";
  #endif
  const auto &kind = value->kind;
  switch (kind.tag) {
    case KOOPA_RVT_RETURN:
      // 访问 return 指令
      Visit(kind.data.ret);
      break;
    case KOOPA_RVT_INTEGER: {
      // 访问 integer 指令
      Visit(kind.data.integer);
      break;
    }
    case KOOPA_RVT_BINARY: {
      Visit(kind.data.binary);
      break;
    }
    case KOOPA_RVT_ALLOC: {
      VisitLocalAlloc(kind.data.global_alloc);
      break;
    }
    case KOOPA_RVT_STORE: {
      Visit(kind.data.store);
      break;
    }
    case KOOPA_RVT_LOAD: {
      Visit(kind.data.load);
      break;
    }
    case KOOPA_RVT_BRANCH: {
      Visit(kind.data.branch);
      break;
    }
    case KOOPA_RVT_JUMP: {
      Visit(kind.data.jump);
      break;
    }
    case KOOPA_RVT_CALL: {
      Visit(kind.data.call);
      break;
    }
    case KOOPA_RVT_GLOBAL_ALLOC: {
      VisitGlobalAlloc(kind.data.global_alloc);
      break;
    }
    default:
      // cout << "false val!!!" << kind.tag << endl;
      assert(false);
  }
}

void VisitGlobalAlloc(const koopa_raw_global_alloc_t &alloc) {
  string var_name = "global_var_" + to_string(global_var_cnt);
  global_var_cnt++;
  cout << "\t.data\n";  
  cout << "\t.globl\t" << var_name << endl;
  cout << var_name << ":" << endl;
  cout << "\t.word\t" << alloc.init->kind.data.integer.value << endl;
  global_var[long(&alloc)] = var_name;
}

void Visit(const koopa_raw_call_t &call) {
  int param_cnt = call.args.len;
  for(int i = 0; i < param_cnt; i++) {
    koopa_raw_value_t param_value = reinterpret_cast<koopa_raw_value_t>(call.args.buffer[i]);
    string param_reg_name = getRegName(param_value);
    if(i < 8) {
      cout << "\tmv\t\t" << "a" << to_string(i) << ",\t" << param_reg_name << endl;
    }
    else {
      cout << "\tsw\t\t" << param_reg_name << ",\t" << 4 * (i - 8) << "(sp)\n";
    }
    releaseReg(param_reg_name);
  }
  string s(call.callee->name);
  cout << "\tcall\t" << s.substr(1) << endl;
  if(call.callee->ty->data.function.ret->tag != KOOPA_RTT_UNIT) {
    string var_name = getStoredAddr(long(&call));
    cout << "\tsw\t\ta0,\t" << var_name << endl;
  }
}

void Visit(const koopa_raw_branch_t &branch) {
  string cond_reg = getRegName(branch.cond);
  cout << "\tbnez\t\t" << cond_reg << ",\t" << ((string)branch.true_bb->name).substr(1) << endl;
  cout << "\tj\t\t" << ((string)branch.false_bb->name).substr(1) << endl;
  releaseReg(cond_reg);
}

void Visit(const koopa_raw_jump_t &jump) {
  cout << "\tj\t\t" << ((string)jump.target->name).substr(1) << endl;
}

void Visit(const koopa_raw_load_t &load) {
  #ifdef _DEBUG
  cout << "visit load, loaded " << load.src->name <<  "\n";
  cout << "visit load, loaded " << load.src->kind.tag <<  "\n";
  #endif
  string regName = getNextReg();
  string var_addr = getStoredAddr(long(&(load.src->kind.data)));
  if(is_global(long(&(load.src->kind.data)))) {
    cout << "\tlw\t\t" << regName << ",\t" << "0(" << var_addr << ")" << endl;
    releaseReg(var_addr);
  }
  else {
    cout << "\tlw\t\t" << regName << ",\t" << var_addr << endl;
  }
  string dest_addr = getStoredAddr(long(&load));
  cout << "\tsw\t\t" << regName << ",\t" << dest_addr << endl;
  releaseReg(regName);
}

void Visit(const koopa_raw_store_t &store) {
  #ifdef _DEBUG
  cout << "visit store, stored " << store.dest->name <<  "\n";
  #endif
  string var_addr = getStoredAddr(long(&(store.dest->kind.data)));
  string regName = getRegName(store.value);
  if(is_global(long(&(store.dest->kind.data)))) {
    cout << "\tsw\t\t" << regName << ",\t" << "0(" << var_addr << ")" << endl;
    releaseReg(var_addr);
  }
  else {
    cout << "\tsw\t\t" << regName << ",\t" << var_addr << endl;
  }
  releaseReg(regName);
}

void VisitLocalAlloc(const koopa_raw_global_alloc_t &alloc) {
  #ifdef _DEBUG
  cout << "visit alloc" << endl;
  #endif
  // getStoredAddr()
}

void Visit(const koopa_raw_return_t &ret) {
  #ifdef _DEBUG
  cout << "visit ret:\n";
  #endif
  if (&(ret.value->ty) == 0){
    cout << epilogue << endl;
    cout << "\tret" << endl;
    return;
  }
  auto kind = ret.value->kind;
  switch(kind.tag) {
    case KOOPA_RVT_INTEGER:{
      cout << "\tli\ta0,\t" << ret.value->kind.data.integer.value << endl;
      cout << epilogue;
      cout << "\tret" << endl;
      break;
    }
    default: {
      string regName = getRegName(ret.value);
      cout << "\tmv\t\ta0, " << regName << endl;
      cout << epilogue;
      cout << "\tret\n";
      releaseReg(regName);
    }
  }

}

void Visit(const koopa_raw_integer_t &int_val) {
  #ifdef _DEBUG
  cout << "visit int_val:" << int_val.value << endl;
  #endif
  cout << "visit int_val:" << int_val.value << endl;
  // cout << "\tli\t" << regName  << ",\t" << int_val.value << endl;
}

void Visit(const koopa_raw_binary_t &bin_val) {
  #ifdef _DEBUG
  cout << "visit bin_val:" << endl;
  #endif

  string lhsName, rhsName, regName;

  lhsName = getRegName(bin_val.lhs);
  rhsName = getRegName(bin_val.rhs);
  regName = getNextReg();

  switch(bin_val.op) {
    case KOOPA_RBO_NOT_EQ: {
      cout << "\txor\t\t" << regName << ",\t" << lhsName << ",\t" << rhsName << endl;
      cout << "\tsnez\t" << regName << ",\t" << regName << endl;
      break;
    }
    case KOOPA_RBO_EQ: {
      cout << "\txor\t\t" << regName << ",\t" << lhsName << ",\t" << rhsName << endl;
      cout << "\tseqz\t" << regName << ",\t" << regName << endl;
      break;
    }
    case KOOPA_RBO_GT: {
      cout << "\tsgt\t\t" << regName << ",\t" << lhsName << ",\t" << rhsName << endl;
      break;
    }
    case KOOPA_RBO_LT: {
      cout << "\tslt\t\t" << regName << ",\t" << lhsName << ",\t" << rhsName << endl;
      break;
    }
    case KOOPA_RBO_GE: {
      cout << "\tslt\t\t" << regName << ",\t" << lhsName << ",\t" << rhsName << endl;
      cout << "\tseqz\t" << regName << ",\t" << regName << endl;
      break;
    }
    case KOOPA_RBO_LE: {
      cout << "\tsgt\t\t" << regName << ",\t" << lhsName << ",\t" << rhsName << endl;
      cout << "\tseqz\t" << regName << ",\t" << regName << endl;
      break;
    }
    case KOOPA_RBO_ADD: {
      cout << "\tadd\t\t" << regName << ",\t" << lhsName << ",\t" << rhsName << endl; 
      break;
    }
    case KOOPA_RBO_SUB: {
      cout << "\tsub\t\t" << regName << ",\t" << lhsName << ",\t" << rhsName << endl;
      break;
    }
    case KOOPA_RBO_MUL: {
      cout << "\tmul\t\t" << regName << ",\t" << lhsName << ",\t" << rhsName << endl;
      break;
    }
    case KOOPA_RBO_DIV: {
      cout << "\tdiv\t\t" << regName << ",\t" << lhsName << ",\t" << rhsName << endl;
      break;
    }
    case KOOPA_RBO_MOD: { 
      cout << "\trem\t\t" << regName << ",\t" << lhsName << ",\t" << rhsName << endl;
      break;
    }
    case KOOPA_RBO_AND: {
      cout << "\tand\t\t" << regName << ",\t" << lhsName << ",\t" << rhsName << endl;
      break;
    }
    case KOOPA_RBO_OR: {
      cout << "\tor\t\t" << regName << ",\t" << lhsName << ",\t" << rhsName << endl;
      break;
    }
    case KOOPA_RBO_XOR: {
      cout << "xor"; 
      break;
    }
    case KOOPA_RBO_SHL: {
      cout << "shl"; 
      break;
    }
    case KOOPA_RBO_SHR: {
      cout << "shr"; 
      break;
    }
    case KOOPA_RBO_SAR: {
      cout << "sar"; 
      break;
    }
    default:
      // cout << "false op!!!" << bin_op << endl;
      assert(false);
  }

  string save_addr = getStoredAddr(long(&bin_val));
  cout << "\tsw\t\t" << regName << ",\t" << save_addr << endl;
  releaseReg(lhsName);
  releaseReg(rhsName);
  releaseReg(regName);
}
